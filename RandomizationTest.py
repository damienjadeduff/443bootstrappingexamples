from __future__ import print_function # Use Python 3 print when in Python 2
import numpy
import numpy.random
from matplotlib import pyplot

data1 = [5.7,6.1,6.2,5.3,5.9,7.8,18.9,5.6,10.3,5.8]
data2 = [18.0,16.6,6,6.1,5.4,7.7,6.6,5.5,5.4,5.1]

#data2 = [x+10 for x in data2] # what happens now?
#for i in range(0,5):
#    data2[i] += 100 # what happens now?

mean1 = numpy.mean(data1)
mean2 = numpy.mean(data2)

difference = mean2 - mean1

mixed = data1+data2

num_samples = 100000
nbins = 50

differences=[]

for n in range(num_samples):
    mixed = numpy.random.permutation(mixed)
    sample1 = mixed[0:len(data1)]
    sample2 = mixed[len(data1):len(data1)+len(data2)]
    bsmean1 = numpy.mean(sample1)
    bsmean2 = numpy.mean(sample2)
    differences.append(bsmean2-bsmean1)

pyplot.hist(differences,bins=nbins,normed=True)
pyplot.ylabel("Normalised Frequency")
pyplot.xlabel("Difference in Mean")
pyplot.title("Estimated distribution of difference in mean")
pyplot.show()

lower_2tail = numpy.percentile(differences,0.5)
upper_2tail = numpy.percentile(differences,99.5)

print("The actual difference in means is",difference)

print("Two tailed thresholds are",lower_2tail,"and",upper_2tail)

if difference > upper_2tail or difference < lower_2tail:
    print("A 2 tailed test with p=0.01 finds the two samples guilty of being different.")
else:
    print("A 2 tailed test with p=0.01 finds the two samples could be from a distribution with the same mean.")

upper_1tail = numpy.percentile(differences,99.0)

print("One tailed upper threshold is",upper_1tail)

if difference > upper_1tail:
    print("A 1 tailed test with p=0.01 finds the two samples guilty of being different.")
else:
    print("A 1 tailed test with p=0.01 finds the two samples could be from a distribution with the same mean.")
