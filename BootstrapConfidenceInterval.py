import numpy
import numpy.random
import matplotlib
from matplotlib import pyplot

data = [5.7,6.1,6.2,5.3,5.9,7.8,18.9,5.6,10.3,5.8]
num_samples = 100000
nbins = 50

means=[]

for n in range(num_samples):
    resampled = numpy.random.choice(data,len(data))
    means.append(numpy.mean(resampled))

pyplot.hist(means,bins=nbins,range=[0,max(data)],normed=True)
pyplot.ylabel("Normalized Frequency")
pyplot.xlabel("Mean of Bootstrap Sample")
pyplot.title("Bootstrap Distribution of Mean")
pyplot.show()

mean = numpy.mean(data)
lower = numpy.percentile(means,0.5)
upper = numpy.percentile(means,99.5)

pyplot.plot([0],[mean],"r*")
pyplot.errorbar([0],[mean],xerr=0.0,yerr=[[abs(lower-mean)],[abs(upper-mean)]])
pyplot.xticks([])
pyplot.ylabel("Mean of Value of Interest")
pyplot.title("Confidence Interval in Mean Calculated from Data\nusing Bootstrap Method")
pyplot.ylim([0.0,max(data)])
pyplot.show()
